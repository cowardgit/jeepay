package com.jeequan.jeepay.pay.channel.allinpay;

import com.jeequan.jeepay.core.constants.CS;
import com.jeequan.jeepay.core.entity.PayOrder;
import com.jeequan.jeepay.pay.channel.AbstractPaymentService;
import com.jeequan.jeepay.pay.model.MchAppConfigContext;
import com.jeequan.jeepay.pay.rqrs.AbstractRS;
import com.jeequan.jeepay.pay.rqrs.payorder.UnifiedOrderRQ;
import com.jeequan.jeepay.pay.util.PaywayUtil;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;

import static jdk.nashorn.internal.runtime.regexp.joni.Config.log;

/**
 * 通联支付的支付接口
 */
@Service
@Slf4j
public class AllinpayPaymentService extends AbstractPaymentService {
    @Override
    public String getIfCode() {
        return CS.IF_CODE.ALLINPAY;
    }

    @Override
    public boolean isSupport(String wayCode) {
        //这里限制支付方式 todo
        boolean payWay = false;
        switch (wayCode) {
            case CS.PAY_WAY_CODE.ALI_QR:
                payWay = true;
                break;
            case CS.PAY_WAY_CODE.WX_NATIVE:
                payWay = true;
                break;
            default:
        }
        return payWay;
    }

    @Override
    public String preCheck(UnifiedOrderRQ bizRQ, PayOrder payOrder) {
        log.info("allinpay precheck,bizRQ:{}", bizRQ);
        log.info("allinpay precheck,payOrder:{}", payOrder);
        return null;
    }

    @Override
    public AbstractRS pay(UnifiedOrderRQ bizRQ, PayOrder payOrder, MchAppConfigContext mchAppConfigContext) throws Exception {
        return PaywayUtil.getRealPaywayService(this, payOrder.getWayCode()).pay(bizRQ, payOrder, mchAppConfigContext);
    }
}
