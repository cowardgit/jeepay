/*
 * Copyright (c) 2021-2031, 河北计全科技有限公司 (https://www.jeequan.com & jeequan@126.com).
 * <p>
 * Licensed under the GNU LESSER GENERAL PUBLIC LICENSE 3.0;
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * <p>
 * http://www.gnu.org/licenses/lgpl.html
 * <p>
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.jeequan.jeepay.pay.channel.allinpay.payway;

import org.springframework.stereotype.Service;

/*
 * 微信 QR支付
 * 通联支付
 */
@Service("allinpayPaymentByWxNativeService") //Service Name需保持全局唯一性
public class WxNative extends AllinpayCommon {

    public WxNative() {
        this.allinpayPayType = "W01";
    }


}
