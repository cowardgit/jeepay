package com.jeequan.jeepay.pay.channel.allinpay.kits;

import com.jeequan.jeepay.core.model.params.allinpay.AllinpayNormalMchParams;
import com.jeequan.jeepay.pay.rqrs.msg.ChannelRetMsg;

import java.lang.reflect.Field;
import java.lang.reflect.Method;
import java.util.Map;
import java.util.TreeMap;

public class SybBaseService {

    protected AllinpayNormalMchParams allinpayNormalMchParams;

    public SybBaseService(AllinpayNormalMchParams allinpayNormalMchParams) {
        this.allinpayNormalMchParams = allinpayNormalMchParams;
    }



    public static Map<String, String> handleResult(String result, String signType, String md5key) throws Exception {
        System.out.println("ret:" + result);
        Map map = SybUtil.json2Obj(result, Map.class);
        if (map == null) {
            throw new Exception("返回数据错误");
        }
        if ("SUCCESS".equals(map.get("retcode"))) {
            TreeMap tmap = new TreeMap();
            tmap.putAll(map);
            String appkey = "";
            if (signType.equals("RSA"))
                appkey = SybConstants.SYB_RSATLPUBKEY;
            else if (signType.equals("SM2"))
                appkey = SybConstants.SYB_SM2TLPUBKEY;
            else
                appkey = md5key;
            if (SybUtil.validSign(tmap, appkey, signType)) {
                System.out.println("签名成功");
                return map;
            } else {
                throw new Exception("验证签名失败");
            }

        } else {
            throw new Exception(map.get("retmsg").toString());
        }
    }

    public static Object getValueByFieldName(String fieldName, Object object) {
        String firstLetter = fieldName.substring(0, 1).toUpperCase();
        String getter = "get" + firstLetter + fieldName.substring(1);
        try {
            Method method = object.getClass().getMethod(getter, new Class[]{});
            Object value = method.invoke(object, new Object[]{});
            return value;
        } catch (Exception e) {
            return null;
        }

    }


}
