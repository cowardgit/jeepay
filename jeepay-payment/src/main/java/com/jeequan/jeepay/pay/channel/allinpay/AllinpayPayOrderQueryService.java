package com.jeequan.jeepay.pay.channel.allinpay;

import cn.hutool.core.date.DateUnit;
import cn.hutool.core.date.DateUtil;
import com.jeequan.jeepay.core.constants.CS;
import com.jeequan.jeepay.core.entity.PayOrder;
import com.jeequan.jeepay.core.model.params.allinpay.AllinpayNormalMchParams;
import com.jeequan.jeepay.pay.channel.IPayOrderQueryService;
import com.jeequan.jeepay.pay.channel.allinpay.kits.*;
import com.jeequan.jeepay.pay.model.MchAppConfigContext;
import com.jeequan.jeepay.pay.rqrs.msg.ChannelRetMsg;
import com.jeequan.jeepay.pay.rqrs.payorder.payway.AliQrOrderRS;
import com.jeequan.jeepay.pay.service.ConfigContextQueryService;
import com.jeequan.jeepay.pay.util.ApiResBuilder;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.time.temporal.ChronoUnit;
import java.util.Date;
import java.util.Map;

@Service
@Slf4j
public class AllinpayPayOrderQueryService implements IPayOrderQueryService {
    @Autowired
    private ConfigContextQueryService configContextQueryService;

    @Override
    public String getIfCode() {
        return CS.IF_CODE.ALLINPAY;
    }

    @Override
    public ChannelRetMsg query(PayOrder payOrder, MchAppConfigContext mchAppConfigContext) throws Exception {
//        return ChannelRetMsg.confirmFail();
        AllinpayNormalMchParams params = (AllinpayNormalMchParams) configContextQueryService.queryNormalMchParams(mchAppConfigContext.getMchNo(), mchAppConfigContext.getAppId(), getIfCode());

        SybQueryService sybService = new SybQueryService(params);
        SybQueryParams sybQueryParams = SybQueryParams.builder()
                .reqsn(payOrder.getPayOrderId())
                .build();
        ChannelRetMsg channelRetMsg = new ChannelRetMsg();
        Map<String, String> resMap = sybService.query(sybQueryParams, channelRetMsg); //里面去包装
        log.debug("接口返回"+resMap.toString());

        // 构造函数响应数据
        if (resMap == null) {
            return null;
        }
        if (!resMap.containsKey("trxstatus")) {
            channelRetMsg.setChannelState(ChannelRetMsg.ChannelState.WAITING);
            return channelRetMsg;
        }
        int sybTrxStatus = new AllinpayTrxStatus(resMap.get("trxstatus")).getCodeRet();
        if (sybTrxStatus == AllinpayTrxStatus.SUCCESS) {
            return ChannelRetMsg.confirmSuccess(resMap.get("chnltrxid"));
        } else if (sybTrxStatus == AllinpayTrxStatus.PENDING) {
            return ChannelRetMsg.waiting();
        } else {
            return ChannelRetMsg.confirmFail();
        }
    }


}
