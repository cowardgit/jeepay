package com.jeequan.jeepay.pay.channel.allinpay.kits;

import com.jeequan.jeepay.core.model.params.allinpay.AllinpayNormalMchParams;
import com.jeequan.jeepay.pay.rqrs.msg.ChannelRetMsg;

import java.lang.reflect.Field;
import java.lang.reflect.Method;
import java.util.Map;
import java.util.TreeMap;

public class SybPayService extends SybBaseService {

    public SybPayService(AllinpayNormalMchParams allinpayNormalMchParams) {
        super(allinpayNormalMchParams);
    }

    /**
     * 通联收银宝请求接口
     *
     * @param sybPayParams
     * @param channelRetMsg
     * @return
     * @throws Exception
     */
    public Map<String, String> pay(SybPayParams sybPayParams, ChannelRetMsg channelRetMsg) throws Exception {
        HttpConnectionUtil http = new HttpConnectionUtil(SybConstants.SYB_APIURL + "/unitorder/pay");
        http.init();
        TreeMap<String, String> params = new TreeMap<String, String>();
        params.put("cusid", allinpayNormalMchParams.getCusid());
        params.put("appid", allinpayNormalMchParams.getAppId());
        params.put("version", SybConstants.VERSION);
        params.put("trxamt", String.valueOf(sybPayParams.getTrxamt()));

        Field[] fields = sybPayParams.getClass().getDeclaredFields();
        for (Field field : fields) {
            String fieldName = field.getName();
            if (getValueByFieldName(fieldName, sybPayParams) != null) {
                params.put(fieldName, getValueByFieldName(fieldName, sybPayParams).toString());
            }
        }
        params.put("randomstr", SybUtil.getValidatecode(8));
//        params.put("signtype", SybConstants.SIGN_TYPE);
        String appkey = allinpayNormalMchParams.getKeys(); //获取密钥
        String signType = allinpayNormalMchParams.getSignType();  //签名方式
        params.put("sign", SybUtil.unionSign(params, appkey, signType));
        byte[] bys = http.postParams(params, true);
        String result = new String(bys, "UTF-8");
        Map<String, String> map;
        try {
            map = handleResult(result, signType, appkey);
        } catch (Exception exception) {
            channelRetMsg.setChannelState(ChannelRetMsg.ChannelState.CONFIRM_FAIL); //确认失败
            channelRetMsg.setChannelErrCode("");
            channelRetMsg.setChannelErrMsg(exception.getMessage());
            return null;
        }
        return map;
    }


}
