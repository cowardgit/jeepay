package com.jeequan.jeepay.pay.channel.allinpay.kits;

import com.alibaba.fastjson.JSONObject;
import com.jeequan.jeepay.core.model.params.allinpay.AllinpayNormalMchParams;
import com.jeequan.jeepay.pay.rqrs.msg.ChannelRetMsg;
import org.springframework.http.ResponseEntity;

import java.util.Map;

public class SybNoticeService extends SybBaseService {
    public SybNoticeService(AllinpayNormalMchParams allinpayNormalMchParams) {
        super(allinpayNormalMchParams);
    }

    public ChannelRetMsg allinpayNotify(JSONObject jsonObject) throws Exception {
        String signType;
        if (!jsonObject.containsKey("signtype")) {
            signType = "MD5";
        }
        signType = jsonObject.getString("signtype");
        String appkey = allinpayNormalMchParams.getKeys();
        Map<String, String> map = handleResult(JSONObject.toJSONString(jsonObject), signType, appkey);
        ChannelRetMsg result = new ChannelRetMsg();
        result.setChannelOrderId(map.get("chnltrxid"));
        result.setChannelState(ChannelRetMsg.ChannelState.WAITING);
        if (map.containsKey("trxstatus")) {
            int sybTrxStatus = new AllinpayTrxStatus(map.get("trxstatus")).getCodeRet();
            if (sybTrxStatus == AllinpayTrxStatus.SUCCESS) {
                result.setChannelState(ChannelRetMsg.ChannelState.CONFIRM_SUCCESS);
            } else if (sybTrxStatus == AllinpayTrxStatus.PENDING) {

            } else {
                result.setChannelState(ChannelRetMsg.ChannelState.CONFIRM_FAIL);
            }
        }
        return result;
    }
}
