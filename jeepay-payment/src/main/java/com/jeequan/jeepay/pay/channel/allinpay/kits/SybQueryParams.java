package com.jeequan.jeepay.pay.channel.allinpay.kits;

import lombok.Builder;
import lombok.Getter;

@Getter
@Builder
public class SybQueryParams {
    private final String reqsn;
    private final String trxid;

}
