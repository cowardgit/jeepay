package com.jeequan.jeepay.pay.channel.allinpay.kits;

public class AllinpayTrxStatus {
    private String trxstatus;
    public static int SUCCESS = 0;
    public static int FAIL = 1;
    public static int PENDING = 2;

    public AllinpayTrxStatus(String code) {
        this.trxstatus = code;
    }

    public int getCodeRet() {
        if (trxstatus.equals("0000")) {
            return SUCCESS;
        } else if (trxstatus.equals("2008") || trxstatus.equals("2000")) {
            return PENDING;
        } else {
            return FAIL;
        }
    }
}
