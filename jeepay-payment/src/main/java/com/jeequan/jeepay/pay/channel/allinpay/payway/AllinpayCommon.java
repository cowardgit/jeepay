package com.jeequan.jeepay.pay.channel.allinpay.payway;

import cn.hutool.core.date.DatePattern;
import cn.hutool.core.date.DateTime;
import cn.hutool.core.date.DateUnit;
import cn.hutool.core.date.DateUtil;
import com.alibaba.fastjson.JSONObject;
import com.alipay.api.domain.AlipayTradePrecreateModel;
import com.alipay.api.domain.Data;
import com.alipay.api.request.AlipayTradePrecreateRequest;
import com.alipay.api.response.AlipayTradePrecreateResponse;
import com.jeequan.jeepay.core.constants.CS;
import com.jeequan.jeepay.core.entity.PayOrder;
import com.jeequan.jeepay.core.model.params.allinpay.AllinpayNormalMchParams;
import com.jeequan.jeepay.core.utils.AmountUtil;
import com.jeequan.jeepay.pay.channel.alipay.AlipayKit;
import com.jeequan.jeepay.pay.channel.allinpay.AllinpayPaymentService;
import com.jeequan.jeepay.pay.channel.allinpay.kits.SybPayParams;
import com.jeequan.jeepay.pay.channel.allinpay.kits.SybPayService;
import com.jeequan.jeepay.pay.model.MchAppConfigContext;
import com.jeequan.jeepay.pay.rqrs.AbstractRS;
import com.jeequan.jeepay.pay.rqrs.msg.ChannelRetMsg;
import com.jeequan.jeepay.pay.rqrs.payorder.UnifiedOrderRQ;
import com.jeequan.jeepay.pay.rqrs.payorder.payway.AliQrOrderRQ;
import com.jeequan.jeepay.pay.rqrs.payorder.payway.AliQrOrderRS;
import com.jeequan.jeepay.pay.rqrs.payorder.payway.WxJsapiOrderRS;
import com.jeequan.jeepay.pay.util.ApiResBuilder;

import java.time.temporal.ChronoUnit;
import java.util.Date;
import java.util.Map;

/**
 * 通联支付的一些方法
 */
public class AllinpayCommon extends AllinpayPaymentService {
    protected String allinpayPayType;

    @Override
    public AbstractRS pay(UnifiedOrderRQ rq, PayOrder payOrder, MchAppConfigContext mchAppConfigContext) throws Exception {

        AllinpayNormalMchParams params = (AllinpayNormalMchParams) configContextQueryService.queryNormalMchParams(mchAppConfigContext.getMchNo(), mchAppConfigContext.getAppId(), getIfCode());

        SybPayService sybService = new SybPayService(params);
        SybPayParams sybPayParams = SybPayParams.builder()
                .trxamt(payOrder.getAmount())
                .reqsn(payOrder.getPayOrderId())
                .paytype(this.allinpayPayType)
                .body(payOrder.getSubject())
                .remark(payOrder.getExtParam())
                .validtime(DateUtil.between(payOrder.getExpiredTime(), new Date(), DateUnit.of(ChronoUnit.SECONDS)))
                .acct(payOrder.getChannelUser())
                .notify_url(getNotifyUrl())
                .build();
        AliQrOrderRS res = ApiResBuilder.buildSuccess(AliQrOrderRS.class);
        ChannelRetMsg channelRetMsg = new ChannelRetMsg();
        res.setChannelRetMsg(channelRetMsg);
        channelRetMsg.setChannelState(ChannelRetMsg.ChannelState.UNKNOWN);  //这里要判断如果是被扫，就不能这样写 todo
        assert res != null;
        Map<String, String> resMap = sybService.pay(sybPayParams, channelRetMsg); //里面去包装
        // 构造函数响应数据
        if (resMap == null) {
            return res;
        }
        res.setCodeUrl(resMap.get("payinfo"));

        return res;
    }

}
