package com.jeequan.jeepay.pay.channel.allinpay.kits;

import lombok.Builder;
import lombok.Getter;

@Getter
@Builder
public class SybPayParams {
    //long trxamt, String reqsn, String paytype, String body, String remark, String acct, String validtime, String notify_url, String limit_pay,
    //                                   String idno, String truename, String asinfo, String sub_appid, String goods_tag, String benefitdetail, String chnlstoreid, String subbranch, String extendparams, String cusip, String fqnum

    private final long trxamt;
    private final String reqsn;
    private final String paytype;
    private final String body;
    private final String remark;
    private final String acct;
    private final long validtime;
    private final String limit_pay;
    private final String idno;
    private final String truename;
    private final String asinfo;
    private final String sub_appid;
    private final String goods_tag;
    private final String benefitdetail;
    private final String chnlstoreid;
    private final String subbranch;
    private final String extendparams;
    private final String cusip;
    private final String fqnum;
    private final String notify_url;
    private final String front_url;

}
