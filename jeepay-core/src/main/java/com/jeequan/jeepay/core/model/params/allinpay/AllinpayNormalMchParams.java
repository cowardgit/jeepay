package com.jeequan.jeepay.core.model.params.allinpay;

import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONObject;
import com.jeequan.jeepay.core.model.params.NormalMchParams;
import com.jeequan.jeepay.core.model.params.alipay.AlipayNormalMchParams;
import com.jeequan.jeepay.core.utils.StringKit;
import lombok.Data;
import org.apache.commons.lang3.StringUtils;

/**
 * 通联支付
 * todo 创建model
 */
@Data
public class AllinpayNormalMchParams extends NormalMchParams {

    /**
     * 是否沙箱环境
     */
    private Byte sandbox;

    /**
     * appId
     */
    private String appId;

    /**
     * cusid
     */
    private String cusid;

    /**
     * keys
     */
    private String keys; //用来保存密钥

    /**
     * 签名方式
     **/
    private String signType;

    @Override
    public String deSenData() {
        AllinpayNormalMchParams mchParams = this;
        if (StringUtils.isNotBlank(this.keys)) {
            mchParams.setKeys(StringKit.str2Star(this.keys, 4, 4, 6));
        }
        return ((JSONObject) JSON.toJSON(mchParams)).toJSONString();
    }
}
