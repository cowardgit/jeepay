import com.jeequan.jeepay.Jeepay;
import com.jeequan.jeepay.JeepayClient;
import com.jeequan.jeepay.exception.JeepayException;
import com.jeequan.jeepay.model.PayOrderCreateReqModel;
import com.jeequan.jeepay.model.PayOrderQueryReqModel;
import com.jeequan.jeepay.request.PayOrderCreateRequest;
import com.jeequan.jeepay.request.PayOrderQueryRequest;
import com.jeequan.jeepay.response.PayOrderCreateResponse;
import com.jeequan.jeepay.response.PayOrderQueryResponse;
import config.AllinpayConfig;
import org.junit.Test;

import java.security.SecureRandom;
import java.util.Date;

public class PayTest {

    private final JeepayClient jeepayClient;

    public PayTest(){
        jeepayClient=JeepayClient.getInstance(AllinpayConfig.appId, AllinpayConfig.apiKey,AllinpayConfig.apiBase);
    }


    @Test
    public void unifieldPayOrder() throws JeepayException {
        // 创建客户端
//        JeepayClient jeepayClient = JeepayClient.getInstance(AllinpayConfig.appId, AllinpayConfig.apiKey,AllinpayConfig.apiBase);

        // 构建请求数据
//        String wayCode = "ALI_QR";                           // 支付方式
        String wayCode = "WX_NATIVE";                           // 支付方式
        PayOrderCreateRequest request = new PayOrderCreateRequest();
        PayOrderCreateReqModel model = new PayOrderCreateReqModel();
        model.setMchNo(AllinpayConfig.mchNo);                       // 商户号
        model.setAppId(jeepayClient.getAppId());            // 应用ID
        String orderNo = "mho" + new Date().getTime();
        model.setMchOrderNo(orderNo);                       // 商户订单号
        model.setWayCode(wayCode);                          // 支付方式
        model.setAmount(1l);                                // 金额，单位分
        model.setCurrency("CNY");                           // 币种，目前只支持cny
        model.setClientIp("192.166.1.132");                 // 发起支付请求客户端的IP地址
        model.setSubject("商品标题");                         // 商品标题
        model.setBody("商品描述");                            // 商品描述
        model.setNotifyUrl("https://www.jeequan.com");      // 异步通知地址
        model.setReturnUrl("");                             // 前端跳转地址
//        model.setChannelExtra((wayCode));       // 渠道扩展参数
        model.setExtParam("");                              // 商户扩展参数,回调时原样返回
        request.setBizModel(model);
        // 发起统一下单
        PayOrderCreateResponse response = jeepayClient.execute(request);

        // 验证返回数据签名
        response.checkSign(AllinpayConfig.apiKey);

        // 判断下单是否返回成功
        response.isSuccess(AllinpayConfig.apiKey);
    }

    //mho1652694189174
    @Test
    public void unifieldQueryOrder() throws JeepayException {
        String mchOrderNo="mho1652779659245";
        PayOrderQueryRequest request=new PayOrderQueryRequest();
        PayOrderQueryReqModel model=new PayOrderQueryReqModel();
        model.setAppId(jeepayClient.getAppId());
        model.setMchNo(AllinpayConfig.mchNo);
        model.setMchOrderNo(mchOrderNo);
        request.setBizModel(model);
        PayOrderQueryResponse response=jeepayClient.execute(request);
        response.checkSign(AllinpayConfig.apiKey);

        // 判断下单是否返回成功
        response.isSuccess(AllinpayConfig.apiKey);
        System.out.println("接口返回"+response.toString());
    }
}
